#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <omp.h>

#include "variables.h"
#include "cosmoparam.h"
#include "leesnap.h"
#include "timer.h"
#include "colores.h"

#define NSIZE_INIT 1000

static void Write_Boxes(const type_int NNN, const type_real * fof);

int main(int argc, char **argv)
{
  type_int  NNN;
  double start,end;

  TIMER(start);
  
  init_variables(argc,argv);
  omp_set_nested(1);

  NNN  = atoi(argv[2]);

  //read_gadget();
  cp.lbox = 400.f;

  GREEN("********** IMPORTANTE ***********\n");
  cp.lbox *= POSFACTOR;
  fprintf(stdout,"lbox %g Kpc\n",cp.lbox);
  GREEN("**********************************\n");

  #ifdef ORIGINAL
  read_grup_fof(fof);
  #endif
  read_segment(NNN,fof);

  //Write_Boxes(NNN,fof);

  free(Seg);
  free(P);

  TIMER(end);
  printf("Total time %f\n",end-start);

  return(EXIT_SUCCESS);
}

static void Write_Boxes(const type_int NNN, const type_real * fof)
{

  type_int  i,j,k,c,flag,Tid;
  type_real r[3],boxsize;
  char filename[200];
  FILE *pfbox;
  type_int *nsize;
  type_int **array_share;

  omp_set_num_threads(NTHREADS);

  nsize       = (type_int *)  malloc(NTHREADS*sizeof(type_int));
  array_share = (type_int **) malloc(NTHREADS*sizeof(type_int *));

  for(i=0;i<NTHREADS;i++)
  {
    nsize[i]       = NSIZE_INIT; 
    array_share[i] = (type_int *) malloc(NSIZE_INIT*sizeof(type_int));    
  }

  #pragma omp parallel for num_threads(NTHREADS) \
  schedule(dynamic) default(none) \
  firstprivate(header) private(i,j,k,c,flag,Tid,r, \
  boxsize,filename,pfbox) \
  shared(P,Seg,cp,fof,nsize,array_share,stdout) 
  for(i=0;i<cp.nseg;i++)
  {

    c = 0;
    Tid = omp_get_thread_num();

    if(i%1000==0)
    {
      fprintf(stdout,"%u %u %.4f\n",Tid,i,(float)i/(float)cp.npart);
      fflush(stdout);
    }

    set_directory("box",filename,i,NNN,fof);
    sprintf(filename,"%s/box_gadget_%.4d.bin",filename,i);
    pfbox = fopen(filename,"w");

    for(k=0;k<cp.npart;k++)
    {

      flag = 0;
      for(j=0;j<3;j++)
      {
        r[j] = P[k].Pos[j]-Seg[i].rmin[j];
        #ifdef PERIODIC
        if(r[j]> 0.5*cp.lbox) r[j] -= cp.lbox;
        if(r[j]<-0.5*cp.lbox) r[j] += cp.lbox;
        #endif
        r[j] += Seg[i].rmin[j];

        if((r[j]>Seg[i].rmin[j]) && (r[j]<=Seg[i].rmax[j]))
          flag++;               
      }

      if(flag==3)
      {
      
        array_share[Tid][c] = k;
        c++;

        if(c == nsize[Tid])
        {
          nsize[Tid] += NSIZE_INIT;
          array_share[Tid] = (type_int *) realloc(array_share[Tid],nsize[Tid]*sizeof(type_int));
        } 

      }
    }

    header.npart[1] = c;
    header.npartTotal[1] = c;
    header.num_files = 1;

    boxsize = Seg[i].rmax[0] - Seg[i].rmin[0];
    if((Seg[i].rmax[1] - Seg[i].rmin[1]) > boxsize)
      boxsize = Seg[i].rmax[1] - Seg[i].rmin[1];
    if((Seg[i].rmax[2] - Seg[i].rmin[2]) > boxsize)
      boxsize = Seg[i].rmax[2] - Seg[i].rmin[2];

    header.BoxSize = boxsize;

    flag = sizeof(header);
    fwrite(&flag,sizeof(flag),1,pfbox);
    fwrite(&header,sizeof(header),1,pfbox);
    fwrite(&flag,sizeof(flag),1,pfbox);

    flag = 3*header.npart[1]*sizeof(type_real);
    fwrite(&flag,sizeof(flag),1,pfbox);

    for(k=0;k<header.npart[1]; k++)
    {
      for(j=0;j<3;j++)
      {
        r[j] = P[array_share[Tid][k]].Pos[j]-Seg[i].rmin[j];
        #ifdef PERIODIC
        if(r[j]> 0.5*cp.lbox) r[j] -= cp.lbox;
        if(r[j]<-0.5*cp.lbox) r[j] += cp.lbox;
        #endif
        r[j] += Seg[i].rmin[j];
      }
      fwrite(r,sizeof(type_real),3,pfbox);
    }

    fwrite(&flag,sizeof(flag),1,pfbox);

    #ifdef STORE_VELOCITIES
    flag = 3*header.npart[1]*sizeof(type_real);
    fwrite(&flag,sizeof(flag),1,pfbox);

    for(k=0;k<header.npart[1]; k++)
      fwrite(P[array_share[Tid][k]].Vel,sizeof(type_real),3,pfbox);

    fwrite(&flag,sizeof(flag),1,pfbox);
    #endif

    #ifdef STORE_IDS
    flag = header.npart[1]*sizeof(type_int);
    fwrite(&flag,sizeof(flag),1,pfbox);

    for(k=0;k<header.npart[1]; k++)
      fwrite(&P[array_share[Tid][k]].id,sizeof(type_int),1,pfbox);

    fwrite(&flag,sizeof(flag),1,pfbox);    
    #endif

    fclose(pfbox);

  }

  for(i=0;i<NTHREADS;i++)
    free(array_share[i]);

  free(array_share);
  free(nsize);

  return;

}

